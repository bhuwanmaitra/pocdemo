﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(POCDemo.Startup))]
namespace POCDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
