USE [AdventureWorks2014]
GO
/****** Object:  StoredProcedure [dbo].[uspGetProductList]    Script Date: 2/12/2018 03:22:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspGetProductList]
  @NumberOfRows int =20,
  @PageStartIndex int =10
  
AS
BEGIN
    SET NOCOUNT ON;
SELECT * FROM Sample_View AS P
ORDER BY P.ProductID
OFFSET @PageStartIndex ROWS
FETCH NEXT @NumberOfRows ROWS ONLY
END;


GO
/****** Object:  StoredProcedure [dbo].[uspGetProductListByProductName]    Script Date: 2/12/2018 03:22:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec uspGetProductListByProductName 
CREATE PROCEDURE [dbo].[uspGetProductListByProductName] 
    @SearchText varchar(50) =''
    
AS
if(@SearchText ='')

    SELECT * from Sample_View
	else
	SELECT * from Sample_View
	where Name like '%'+@SearchText+'%'

GO
/****** Object:  View [dbo].[Sample_View]    Script Date: 2/12/2018 03:22:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[Sample_View]
AS

SELECT        Production.Product.ProductID, Production.Product.Name, Production.Product.ProductNumber, Production.Product.Color, Production.Product.Size, 
                         Production.Product.ListPrice, Production.Product.StandardCost, Production.Product.Style, Production.Product.rowguid, Production.ProductPhoto.LargePhoto, 
                         Production.ProductDescription.Description, Production.ProductModelProductDescriptionCulture.CultureID
FROM            Production.Product left JOIN
                         Production.ProductProductPhoto ON Production.Product.ProductID = Production.ProductProductPhoto.ProductID left JOIN
                         Production.ProductPhoto ON Production.ProductProductPhoto.ProductPhotoID = Production.ProductPhoto.ProductPhotoID left JOIN
                         Production.ProductModelProductDescriptionCulture ON 
                         Production.Product.ProductModelID = Production.ProductModelProductDescriptionCulture.ProductModelID  and Production.ProductModelProductDescriptionCulture.CultureID='en' left JOIN
                         Production.ProductDescription ON Production.ProductModelProductDescriptionCulture.ProductDescriptionID = Production.ProductDescription.ProductDescriptionID 




GO
