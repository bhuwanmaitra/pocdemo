﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POCDemo.Models
{
    public class ProductListModel
    {
        public int ProductID { get; set; }
        public string  serachText { get; set; }
        public string  Name { get; set; }
        public string  ProductNumber { get; set; }
        public string  Color { get; set; }
        public string  Size { get; set; }
        public decimal ListPrice { get; set; }
        public decimal StandardCost { get; set; }
        public string Style { get; set; }
        public System.Guid rowguid { get; set; }
        public byte[] LargePhoto { get; set; }
        public string Description { get; set; }
        public List<ProductListModel> ProductListModel_list { get; set; }        
        
    }
}